<?php

class Request{
	
    public static $error;
    
	public static function Field($key){
		if($key != ""):
            $request = $_REQUEST;
			if(isset($request[$key]) && (is_numeric($request[$key]) || ($request[$key] != "" && !empty($request[$key])))):
			
				if (!is_array($request[$key])) :
					if (is_numeric($request[$key])) {
						return floatval($request[$key]);
					}
					else {
						return trim($request[$key]);
					}
				else: 
					return $request[$key];
				endif;
			
			else:
				return false;
			endif;
		else:
			return false;
		endif;	
	}
	public static function Wants($key="trigger"){
	
		$request = $_REQUEST;
		if(isset($request[$key]) && $request[$key] != ""): # && !empty($request[$key])):
			return true;	
		else:
			return false;
		endif;
	}
     public static function getQueryString($omitKey='dir', $omitBlank=false)
    {
        $query_string = "";
        foreach ($_POST as $key => $value) {
			if (is_array($value) && count($value) > 0) {
				if ($query_string == "") {
                 	$i = 0;
					foreach ($value as $index => $item) {
						if ($i == $index) {
							$query_string = $key . "[]=" . urlencode($item);
						}
						else {
							$query_string = $key . "[$index]=" . urlencode($item);
						}
						$i++;
					}
	            }
                else {
					$i = 0;
					foreach ($value as $index => $item) {
						if ($i == $index) {
							$query_string .= "&" . $key . "[]=" . urlencode($item);
						}
						else {
							$query_string .= "&" . $key . "[$index]=" . urlencode($item);
						}
						$i++;
					}
                }
			}
            else if (($omitBlank == false || _trim($value) != "") && $key != $omitKey) {
                if ($query_string == "") {
                    $query_string = $key . "=" . urlencode($value);
                }
                else {
                    $query_string .= "&" . $key . "=" . urlencode($value);
                }
            }
        }
        foreach ($_GET as $key => $value) {
			if (is_array($value) && count($value) > 0) {
				if ($query_string == "") {
                 	$i = 0;
					foreach ($value as $index => $item) {
						if ($i == $index) {
							$query_string = $key . "[]=" . urlencode($item);
						}
						else {
							$query_string = $key . "[$index]=" . urlencode($item);
						}
						$i++;
					}
	            }
                else {
					$i = 0;
					foreach ($value as $index => $item) {
						if ($i == $index) {
							$query_string .= "&" . $key . "[]=" . urlencode($item);
						}
						else {
							$query_string .= "&" . $key . "[$index]=" . urlencode($item);
						}
						$i++;
					}
                }
			}
            else if (($omitBlank == false || _trim($value) != "") && $key != $omitKey) {
                if ($query_string == "") {
                    $query_string = $key . "=" . urlencode($value);
                }
                else {
                    $query_string .= "&" . $key . "=" . urlencode($value);
                }
            }
        }
        return $query_string;      
    }
}