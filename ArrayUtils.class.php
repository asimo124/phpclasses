<?php
class ArrayUtils {
	
	public static function is_equal($obj1, $obj2) {
		
		if (self::compare_to($obj1, $obj2) == true && self::compare_to($obj2, $obj1) == true) {	
			return true;
		}
		return false;
	}
	public static function compare_to($obj1, $obj2)
	{	
		if (self::isHashArr($obj1) && !self::isHashArr($obj2)) {
			return false;
		}
		if (is_array($obj1) && !is_array($obj2)) {
			return false;
		}
		if (is_object($obj1) && !is_object($obj2)) {
			return false;	
		}
		if (is_scalar($obj1) && !is_scalar($obj2)) {
			return false;	
		}
		if (is_object($obj1)) {
			$hasBad = false;
			$hasMembers = false;
			foreach ($obj1 as $key => $value) {
				$hasMembers = true;
				if (!property_exists($obj2, $key)) {
					return false;
				}
				$retVal = self::compare_to($obj1->$key, $obj2->$key);
				if ($retVal == false) {
					$hasBad = true;
					break;	
				}
			}
			if ($hasBad == true) {
				return false;	
			}
			if ($hasMembers == false) {
				$hasMembers = false;
				foreach (array_keys($obj2) as $key) {
					$hasMembers = true;	
				}
				if ($hasMembers == true) {
					return false;	
				}
				return true;
			}
			return true;	
		}
		else if (self::isHashArr($obj1)) {	
			$hasBad = false;
			$hasMembers = false;
			foreach (array_keys($obj1) as $key) {
				$hasMembers = true;
				if (!array_key_exists($key, $obj2)) {
					return false;
				}
				$retVal = self::compare_to($obj1[$key], $obj2[$key]);
				if ($retVal == false) {
					$hasBad = true;
					break;	
				}
			}	
			if ($hasBad == true) {
				return false;	
			}
			if ($hasMembers == false) {
				$hasMembers = false;
				foreach (array_keys($obj2) as $key) {
					$hasMembers = true;
				}
				if ($hasMembers == false) {
					return true;
				}
			}
			return true;	
		}
		else if (is_array($obj1)) {
			if (count($obj1) != count($obj2)) {
				return false;
			}	
			$hasBad = false;
			foreach ($obj1 as $i => $value) {
				$retVal = self::compare_to($value, $obj2[$i]);
				if ($retVal == false) {
					$hasBad = true;
					break;	
				}
			}
			if ($hasBad == true) {
				return false;	
			}	
			return true;
		}
		else if (is_scalar($obj1)) {
			return $obj1 == $obj2;
		}
		return true;
	}
	public static function isHashArr($arr)
	{
		if (!is_array($arr)) {
			return false;	
		}
		$i = 0;
		foreach ($arr as $index => $value) {
			if ($index != $i) {
				return true;	
			}
			$i++;
		}
		return false;
	}
}
?>