<?php
class QueryHandler {

	public $table = "tbl_Items";
	public $key = "id";
	public $Items = array();
	public $sthLoadByID;
	
	function __construct($queryType=array()) {
		global $db_conn;
		$queryLoadByID = "
		select * from $table
		WHERE $key = :$key 
		LIMIT 1 ";
		$this->sthLoadByID = $db_conn->prepare($queryLoadByID);
	}
	# Magic Setters, Getters
	public function __set($property, $value) {
		$this->Items[$property] = $value;
	}
	public function __get($property) {
		if ($property == "Items") {
			return $this->Items;
		}
		else {
			return $this->Items[$property];
		}
	}
	# Load By ID function 
	public function loadByID($ID) {
		$this->sthLoadByID->execute(array("id" => $ID));
		$resultset = $this->sthLoadByID->fetchAll();
		foreach ($resultset[0] as $key => $value) {
			$this->$key = $value;
		}
	}
}
?>