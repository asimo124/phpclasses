<?php
class CrudGenUtility{
	
	public function writeablePathsCreated()
	{
		$Settings = new Settings;
		$Settings->loadSettings();
		if (trim($Settings->settings['TemplatesWriteablePath'] . chr(32)) != "" 
			&& trim($Settings->settings['SrcWriteablePath'] . chr(32)) != ""
			&& trim($Settings->settings['ClassesWriteablePath'] . chr(32)) != "")
		{
			return true;	
		}
		return false;
	
	}
	
	public static function getField($field)
	{
		if (strpos($field, "_") !== false) {
			$fieldArr = explode("_", $field);
			$i = 0;
			$field_str = "";
			foreach ($fieldArr as $item) {
				if ($i > 0) {
					if ($fieldArr > 0) {
						if ($field_str == "") {
							$field_str = ucfirst($item);	
						}
						else {
							$field_str .= " " . ucfirst($item);
						}
					}
				}
				$i++;
			}
			return $field_str;
		}
		else {
			return $field;
		}
	}
	
	public static function createSrcPathIfNotExists($folder)
	{
		$Settings = new Settings();
		$folderArr = explode("/", $folder);
		
		$useFolderArr = array();
		foreach ($folderArr as $GetFolder) {
			if (trim($GetFolder) != "") {
				$useFolderArr[] = $GetFolder;
			}
		}
		
		$get_path = $Settings->settings['SrcWriteablePath'];
		for ($i = 0; $i < count($useFolderArr) - 1; $i++) {
			
			$get_path .= "/" . $useFolderArr[$i];
			if (!file_exists($get_path)) {
				mkdir($get_path, 0775);	
			}
		}
	}
	
	public static function createTemplatesPathIfNotExists($folder)
	{
		$Settings = new Settings();
		$folderArr = explode("/", $folder);
		
		$useFolderArr = array();
		foreach ($folderArr as $GetFolder) {
			if (trim($GetFolder) != "") {
				$useFolderArr[] = $GetFolder;
			}
		}
		
		$get_path = $Settings->settings['TemplatesWriteablePath'];
		for ($i = 0; $i < count($useFolderArr) - 1; $i++) {
			
			$get_path .= "/" . $useFolderArr[$i];
			if (!file_exists($get_path)) {
				mkdir($get_path, 0775);	
			}
		}
	}
	
	public static function createClassesPathIfNotExists($folder)
	{
		$Settings = new Settings();
		$folderArr = explode("/", $folder);
		
		$useFolderArr = array();
		foreach ($folderArr as $GetFolder) {
			if (trim($GetFolder) != "") {
				$useFolderArr[] = $GetFolder;
			}
		}
		
		$get_path = $Settings->settings['ClassesWriteablePath'];
		for ($i = 0; $i < count($useFolderArr) - 1; $i++) {
			
			$get_path .= "/" . $useFolderArr[$i];
			if (!file_exists($get_path)) {
				mkdir($get_path, 0775);	
			}
		}
	}
	
}