<?php

class NewRequest{
	
    public static $error;
    
	public static function Field($key, $default=false){
		if($key != ""):
            $request = $_REQUEST;
			if(isset($request[$key]) && (is_numeric($request[$key]) || ($request[$key] != "" && !empty($request[$key])))):
			
				if (!is_array($request[$key])) :
					//if (is_numeric($request[$key])) {
					//	return floatval($request[$key]);
					//}
					//else {
						return trim($request[$key]);
					//}
				else: 
					return $request[$key];
				endif;
			
			else:
				return $default;
			endif;
		else:
			return $default;
		endif;	
	}

	public static function FieldI($key, $default=-1){
		if($key != ""):
            $request = $_REQUEST;
			if(isset($request[$key]) && (is_numeric($request[$key]) || ($request[$key] != "" && !empty($request[$key])))):
			
				if (!is_array($request[$key])) :
					//if (is_numeric($request[$key])) {
					//	return floatval($request[$key]);
					//}
					//else {
						return intval($request[$key]);
					//}
				else: 
					return $request[$key];
				endif;
			
			else:
				return $default;
			endif;
		else:
			return $default;
		endif;	
	}

	public static function FieldF($key, $default=-1){
		if($key != ""):
            $request = $_REQUEST;
			if(isset($request[$key]) && (is_numeric($request[$key]) || ($request[$key] != "" && !empty($request[$key])))):
			
				if (!is_array($request[$key])) :
					//if (is_numeric($request[$key])) {
					//	return floatval($request[$key]);
					//}
					//else {
						return floatval($request[$key]);
					//}
				else: 
					return $request[$key];
				endif;
			
			else:
				return $default;
			endif;
		else:
			return $default;
		endif;	
	}

	public static function FieldD($key, $default="", $format="Y-m-d", $isWindowsDate=false){
		
		if($key != ""):
            $request = $_REQUEST;
			if(isset($request[$key]) && (is_numeric($request[$key]) || ($request[$key] != "" && !empty($request[$key])))):
			
				if (!is_array($request[$key])) :
					//if (is_numeric($request[$key])) {
					//	return floatval($request[$key]);
					//}
					//else {

						if ($isWindowsDate == true) {
							$date2 = changeMysqlDate($request[$key]);
						}
						else {
							$date2 = $request[$key];
						}
						$retVal = show_date($date2);

						if (show_date($retVal) == "") {
							return $default;
						}
						else {
							return show_date($date2, $format);
						}
					//}
				else: 
					return $request[$key];
				endif;
			
			else:
				return $default;
			endif;
		else:
			return $default;
		endif;	
	}

	public static function FieldB($key, $default=-1){
		if($key != ""):
            $request = $_REQUEST;
			if(isset($request[$key]) && (is_numeric($request[$key]) || ($request[$key] != "" && !empty($request[$key])))):
			
				if (!is_array($request[$key])) :
					//if (is_numeric($request[$key])) {
					//	return floatval($request[$key]);
					//}
					//else {

						if (trim($request[$key]) == "") {
							return -1;
						}
						else if ($request[$key] == 1 && $request[$key] == 0) {
							return $request[$key];
						}
						else {
							return -1;
						}
					//}
				else: 
					return $request[$key];
				endif;
			
			else:
				return $default;
			endif;
		else:
			return $default;
		endif;	
	}

	public static function Wants($key="trigger"){
	
		$request = $_REQUEST;
		if(isset($request[$key]) && $request[$key] != ""): # && !empty($request[$key])):
			return true;	
		else:
			return false;
		endif;
	}
    public static function getQueryString($omitKey='', $omitBlank=false, $specifyRequest='')
    {
        $query_string = "";
        foreach ($_POST as $key => $value) {
            if (($omitBlank == false || _trim($value) != "") && $key != $omitKey) {
                if ($query_string == "") {
                    $query_string = $key . "=" . urlencode($value);
                }
                else if (strpos($query_string, "&" . $key . "=") === false) {
                    $query_string .= "&" . $key . "=" . urlencode($value);
                }
            }
        }
        foreach ($_GET as $key => $value) {
             if (($omitBlank == false || _trim($value) != "") && $key != $omitKey) {
                if ($query_string == "") {
                    $query_string = $key . "=" . urlencode($value);
                }
                else if (strpos($query_string, "&" . $key . "=") === false) {
                    $query_string .= "&" . $key . "=" . urlencode($value);
                }
            }
        }
        return $query_string;      
    }
}