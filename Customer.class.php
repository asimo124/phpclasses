<?php
class Customer extends QueryHandler {

	public $table = "vnd_customers";
	public $key = "id";
	public $sth_sel_active_customers;

	public function __construct($queryType=array()) {

		parent::__construct(array());

		foreach ($queryType as $key) {
			switch ($key) {
				case "Active":
					$querySelActive = "
					SELECT * FROM vnd_customers
					WHERE active = 1 and deleted = 0 
					and id = :id ";
					$this->sth_sel_active_customers = $db_conn->prepare($querySelActive);
					break;
			}
		}
	}

	public function selectActiveCustomers($id) {
		$this->sth_sel_active_customers = $this->sel_active_customers->execute(array("id" => $id));
		return $this->sel_active_customers->fetchAll();
	}
}

## Usage ##

# Call function for main loop
$Orders = $Order->getActiveOrders();

# Instantiate Customer Query Handler Class, pass query type(s) to constructor
$Customer = new Customer(array("Active"));
?>

<?php # Loop through Orders ?>
<?php foreach ($Orders as $getOrder) : ?>

	<?php # call function to get Customers ?>
	<?php $Customers = $Customer->selectActiveCustomers($getOrder['CustomerID']); ?>

	<h3>Order #: <?php echo $getOrder['OrderID']; ?></h3>

	<?php # Loop through customers ?>
	<?php foreach ($Customers as $getCust) : ?>

		<li><?php echo $getCust['first_name'] . " " . $getCust['last_name']; ?></li>
	<?php endforeach; ?>

<?php endforeach; ?>
?>