<?php
class FormValidator {
	public static $validateKey = "";
	public static $returnURL = "";
	public static $rules;
	public static $error;
	public static $sanitize = false;
	public static $error_num = 0;
	public static $table = "";
	public static $key = "";
	public static $key_from = "";
	public static $key_value = 0;
	public static $session_num_keys = "0";
	public static $session_key1 = "";
	public static $session_key2 = "";
	public static $session_key3 = "";
	public static function Validate($sanitize=false) {
		self::$sanitize = intval($sanitize) == 0 ? false : 1;
		# check if fmv key exists
		foreach ($_REQUEST as $key => $value) {
			$pattern = '/fmv_[a-zA-Z0-9]{1,50}/';
			preg_match($pattern, $key, $matches);
			if (count($matches) > 0) {
				if (trim($value) == "") {
					return 0;
				}
				self::$validateKey = $key;
				self::$returnURL = $value;
				break;
			}	
		}
		# check if json exists with key filename
		$path = ValidatePath . self::$validateKey . ".json";
		if (!file_exists($path)) {
			return 0;
		}
		$fh = fopen($path, 'r');
		# load rules array into rules property
		$rules_content = fread($fh, filesize($path));
		self::$rules = json_decode($rules_content);
		self::$table = self::$rules->table;
		self::$key = self::$rules->key;
		if (property_exists(self::$rules, "key_from") == true) {
			self::$key_from = self::$rules->key_from;
		}
		if (property_exists(self::$rules, "session_num_keys") == true) {
			self::$session_num_keys = self::$rules->session_num_keys;
		}
		if (property_exists(self::$rules, "session_key1") == true) {
			self::$session_key1 = self::$rules->session_key1;
		}
		if (property_exists(self::$rules, "session_key2") == true) {
			self::$session_key2 = self::$rules->session_key2;
		}
		if (property_exists(self::$rules, "session_key3") == true) {
			self::$session_key3 = self::$rules->session_key3;
		}
		if (self::$key_from == "Session") {
			switch (self::$session_num_keys) {
				case 1:
					self::$key_value = $_SESSION[self::$rules->session_key1];
					break;
				case 2:
					self::$key_value = $_SESSION[self::$rules->session_key1][self::$rules->session_key2];
					break;
				case 3:
					self::$key_value = $_SESSION[self::$rules->session_key1][self::$rules->session_key2][self::$rules->session_key3];
					break;
				default:
					self::$key_value = $_SESSION[self::$rules->session_key1];
			}
		}
		else {
			self::$key_value = NewRequest::Field(self::$rules->key);
		}
		$finalRetVal = 1;
		foreach (self::$rules->fields as $key => $arr) {
			$retVal = self::validateField($key);
			if ($retVal < 1) {
				$finalRetVal = -1;
				break;
			}
		}
		return $finalRetVal;
	}
	public static function validateField($key) {
		$curRule = self::$rules->fields->$key;
		# Get Values
		$type = $curRule->type;
		$label = $curRule->label;
		$validate = "";

		if (!property_exists($curRule, "validate")) {
			$get_validates = "required";
		}
		else {
			$get_validates = $curRule->validate; 
		}
		$all_validates = explode(",", $get_validates);
		foreach ($all_validates as $index => $get_validate) {
			$all_validates[$index] = trim($get_validate);
		}
		$default = "";
		if (!property_exists($curRule, "default")) {
			switch ($type) {
				case "integer":
				case "float":
					$default = "0";
					break;
			}
		}
		else {
			$default = $curRule->default;
		}
		# Validate by Type
		switch ($type) {
			case "string":
				foreach ($all_validates as $get_validate) {
					$retVal = self::validateString($key, $label, $default, $get_validate);
					if ($retVal == -1) {
						self::$error_num = 1;
						if (CUR_PAGE == self::$returnURL) {
							return -1;
						}
						else {
							
							Redirect(self::$returnURL . "?Message= " . urlencode(self::$error) . "&" . NewRequest::getQueryString());
						}
					}
				}
				break;
			case "integer":
				$retVal = self::validateInt($key, $label, $default);
				if ($retVal == -1) {
					self::$error_num = 2;
					if (CUR_PAGE == self::$returnURL) {
						return -1;
					}
					else {
						
						Redirect(self::$returnURL . "?Message= " . urlencode(self::$error) . "&" . NewRequest::getQueryString());
					}
				}
				break;
			case "float":
				$retVal = self::validateFloat($key, $label, $default);
				if ($retVal == -1) {
					self::$error_num = 3;
					if (CUR_PAGE == self::$returnURL) {
						return -1;
					}
					else {
						
						Redirect(self::$returnURL . "?Message= " . urlencode(self::$error) . "&" . NewRequest::getQueryString());
					}
				}
				break;
			case "date":
				$retVal = self::validateDate($key, $label);
				if ($retVal == -1) {
					self::$error_num = 4;
					if (CUR_PAGE == self::$returnURL) {
						return -1;
					}
					else {
						
						Redirect(self::$returnURL . "?Message= " . urlencode(self::$error) . "&" . NewRequest::getQueryString());
					}
				}
				break;
		}
		return 1;
	}
	public static function validateString($key, $label, $default, $validate="required") {
		global $getdb;
		if ($validate == "required") {
			$value = NewRequest::Field($key);
			$value = trim($value);
			if (self::$sanitize == true) {
				if ($value == "") {
					if ($default != "") {
						$_REQUEST[$key] = $default;
					}
					else {
						$_REQUEST[$key] = $value;
					}
				}
			}
			if ($value == "" && $default == "") {
				self::$error = "You did not enter any information for " . $label . ".";
				return -1;
			}
			
		}
		else if ($validate == "email") {
			$value = NewRequest::Field($key);
			$value = trim($value);
			if (self::$sanitize == true) {
				if ($value == "") {
					$_REQUEST[$key] = $value;
				}
			}
			if (!isValidEmail($value)) {
				self::$error = "The value entered for " . $label . " is not a valid email.";
				return -1;
			}
		}
		else if ($validate == "unique") {
	        $key_value = intval(self::$key_value);
	        $value = NewRequest::Field($key);
	        if ($key_value < 1) {	
				$query = "
				SELECT * FROM " . self::$table . "
				WHERE `" . $key . "` = :" . $key;
				if (self::$table == "tbl_Customers") {
					$query .= " and Deleted=0";
				}

				$data = array();
				$data[$key] = $value;

				$resultset = $getdb->get_results($query, $data);

				if (is_array($resultset)) {
					self::$error = "The " . $label . " you entered already exists on the system.";
					return -1;
				}
	        }
	        else {  	
				$query = "
				SELECT * FROM " . self::$table . "
				WHERE `" . $key . "` = :" . $key . " and `" . self::$key . "` <> " . self::$key_value;
				if (self::$table == "tbl_Customers") {
					$query .= " and Deleted=0";
				}

				$data = array();
				$data[$key] = $value;

				$resultset = $getdb->get_results($query, $data);

				if (is_array($resultset)) {
					self::$error = "The " . $label . " you entered already exists on the system.";
					return -1;
				}			
	        }
		}
		return 1;
	}
	public static function validateInt($key, $label, $default) {
		$default = intval($default);
		$value = NewRequest::Field($key);
		$value = intval($value);
		if ($value < 1) {
			$value = $default;
		}
		if (self::$sanitize == true) {
			$_REQUEST[$key] = $value;
		}
		if ($value < 1) {
			self::$error = "The field " . $label . " must be greater than 0.";
			return -1;
		}
		return 1;
	}
	public static function validateFloat($key, $label, $default) {
		$default = floatval($default);
		$value = NewRequest::Field($key);
		$value = floatval($value);
		if ($value < 0.01) {
			$value = $default;
		}
		if (self::$sanitize == true) {
			$_REQUEST[$key] = $value;
		}
		if ($value < 0.01) {
			self::$error = "The field " . $label . " must be greater than 0.";
			return -1;
		}
		return 1;
	}
	public static function validateDate($key, $label) {
		$value = NewRequest::Field($key);
		$value = show_date($value);
		if (self::$sanitize == true) {
			$_REQUEST[$key] = "";
		}
		if ($value == "") {
			self::$error = "The date " . $label . " is not a valid date.";
			return -1;
		}
		return 1;
	}
}
?>