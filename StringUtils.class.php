<?php
class StringUtils
{
	public static function clean($str)
	{
		$word_str = "";
		for ($i = 0; $i < strlen($str); $i++) {
			$cur_letter = substr($str, $i, 1);
			$cur_char = ord($cur_letter);
			if ($cur_char > 127) {
				logSpecialChars($cur_char, $cur_letter);
			}
			switch ($cur_char)
			{
				case 214:
					$use_str = "&Ouml;";
					break;
				case 252:
					$use_str = "&uuml;";
					break;
				case 143:
					$use_str = "";
					break;
				case 233:
					$use_str = "&eacute;";
					break;
				case 246:
					$use_str = "&ouml;";
					break;
				case 237:
					$use_str = "&iacute;";
					break;
				case 225:
					$use_str = "&aacute;";
					break;
				case 232:
					$use_str = "&egrave;";
					break;
				case 235:
					$use_str = "&euml;";
					break;
				case 231:
					$use_str = "&ccedil;";
					break;
				case 228:
					$use_str = "&auml;";
					break;
				case 243:
					$use_str = "&oacute;";
					break;
				case 242:
					$use_str = "&ograve;";
					break;
				case 248:
					$use_str = "&oslash;";
					break;
				case 201:
					$use_str = "&Eacute;";
					break;
				case 227:
					$use_str = "&atilde;";
					break;
				case 197:
					$use_str = "&Aring;";
					break;
				case 229:
					$use_str = "&aring;";
					break;
				case 214:
					$use_str = "&Ouml;";
					break;
				case 250:
					$use_str = "&uacute;";
					break;
				case 241:
					$use_str = "&ntilde;";
					break;
				case 244:
					$use_str = "&ocirc;";
					break;
				case 193:
					$use_str = "&Aacute;";
					break;
				case 239:
					$use_str = "&iuml;";
					break;
				case 211:
					$use_str = "&Oacute;";
					break;
				case 226:
					$use_str = "&acirc;";
					break;
				case 245:
					$use_str = "&otilde;";
					break;
				case 249:
					$use_str = "&ugrave;";
					break;
				default:
					$use_str = $cur_letter;
			}
			$word_str .= $use_str;	
		}

		return $word_str;
	}
	
	public static function printChar($str, $doEcho=true)
	{
		$arr2 = array();
		for ($i = 0; $i < strlen($str); $i++) {
			$arr2[] = ord(substr($str, $i, 1)) . "\t" . substr($str, $i, 1);
		}
		
		if ($doEcho) {
			preformat2($arr2);	
		}
		return $arr2;
		
	}
	
	public static function toAscii($str, $delimiter='-', $bumpyCase=false) {
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
	
		if ($bumpyCase == true) {
			$word_str = "";
			$wordsArr = explode($delimiter, $clean);
			foreach ($wordsArr as $get_word) {
				$word_str .= ucfirst($get_word);
			}	
			$clean = $word_str;
		}	
		return $clean;
	}
	
	public static function isHashArr($arr)
	{
		if (!is_array($arr)) {
			return false;	
		}
		$i = 0;
		foreach ($arr as $index => $value) {
			if ($index != $i) {
				return true;	
			}
			$i++;
		}
		return false;
	}
	
	
	public static function encodePayload2($payload, $payload_str="")
	{
		if ($payload_str == "" && !is_object($payload) && !is_array($payload)) {
			return "[]";	
		}
		if (is_object($payload)) {
			$payload_str .= "{\n";
			if (count(get_object_vars($payload)) == 0) {
				$payload_str .= "";
			}
			else {
				foreach (get_object_vars($payload) as $key) {
				
					$payload_str .= "'" . $key . "': ";
					$payload_str = self::encodePayload2($payload->$key, $payload_str);
				}
			}
			$payload_str .= "},\n";
			return $payload_str;
		}
		else if (self::isHashArr($payload)) {
			$payload_str .= "{\n";
			if (count(array_keys($payload)) == 0) {
				$payload_str .= "";
			}
			else {
				foreach (array_keys($payload) as $key) {
				
					$payload_str .= "'" . $key . "': ";
					$payload_str = self::encodePayload2($payload[(string)$key], $payload_str);
				}
			}
			$payload_str .= "},\n";
			return $payload_str;
		}
		else if (is_array($payload)) {
			$payload_str .= "[\n";
			if (count($payload) == 0) {
				$payload_str .= "";
			}
			else {
				foreach ($payload as $index => $value) {
					$payload_str = self::encodePayload2($value, $payload_str);
				}
			}
			$payload_str .= "],\n";
			return $payload_str;
		}
		else {
			$payload_str .= "'" . str_replace("'", "\\'", $payload) . "',\n";
			return $payload_str;
		}
	}
	public static function encodePayload($payload)
	{
		$get_payload = self::encodePayload2($payload);
		return substr($get_payload, 0, strlen($get_payload) - 2);
	}
	
}
?>