<?php
class DataItem 
{
	public $data;
    public $table_name = "";
	public $key_field = "";
	
    public $Fields = array();
    public $req_fields_validate = array();
    public $email_fields_validate = array();
    public $unique_fields_validate = array();

	public function __construct($id=-1){
	
		
	}
	public function loadByID($id=-1){
	
		if($id != 0):
			$query = "SELECT * FROM {$this->table_name} WHERE {$this->key_field} = :id ";
			$params["id"] = $id;
			$results = Database::Results($query,$params);
			if(count($results) > 0):
			
				$this->data = $results[0];
				
				return $this->data;	
			else:
				return array();
			endif;
		else:
			return array();
		endif;	
	}
    function loadFromRequest($noOverwrite=false)
	{
		foreach ($this->Fields as $Field) {
		
			# check if has value
			if ($_REQUEST[$Field] = "") {
				$this->data[$Field] = $_REQUEST[$Field];
			}
        }
	}
	
	public function clear() {
		
		$this->data = array();	
	}
	
	public function insert()
	{
		global $db_conn;
		
		unset($this->data[$this->key_field]);
		$array = $this->data;
		
		$fields = array_keys($array);
		foreach ($fields as $index => $get_field) {
			$fields[$index] = "`" . $get_field . "`";
		}
		$fieldList = implode(",",$fields);		
		foreach($array as $field => $value):
			$queryBinds[] = ":".$field;
			$queryParams[":".$field] = $value;		
		endforeach;
		$valueList = implode(",",$queryBinds);
		
		$q = "INSERT INTO {$this->table_name} ($fieldList) VALUES ($valueList)";
		$sth = $db_conn->prepare($q);
		$sth->execute($queryParams);
		
		$result = $db_conn->lastInsertId();
		return $result;	
	}
	public function update()
	{
		if (!isset($this->data[$this->key_field]) || intval($this->data[$this->key_field]) < 1) {
			return false;	
		}
		$id = $this->data[$this->key_field];
		unset($this->data[$this->key_field]);
		
		$array = $this->data;		
		foreach($array as $field => $value):
			$update[] = "`" . $field . "` = :".$field;
			$params[":" . $field] = $value;		
		endforeach;
		
		$base = "UPDATE {$this->table_name} SET";
		$update = implode(",",$update);
		$params[":id"] = $id;
		$the_field = $this->key_field;
		$query = $base." ".$update." "."WHERE $the_field = :id";
		Database::Query($query,$params);
		
		$this->loadByID($id);
		return true;
	}
}  
?>