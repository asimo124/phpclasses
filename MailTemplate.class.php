<?php
class MailTemplate
{
	public static function send($TemplatePath, $FieldsArr, $Subject, $Recipient, $From='', $useSmarty=true)
	{
		global $Global;
		global $Smarty;
		
		$Settings = new Settings();
		if ($From == "") {
			$From = $Global->Settings->AdminEmail;	
		}
		if ($useSmarty == true) {
			
			$Smarty->setTemplateDir(EMAIL_TPL_PATH);
			foreach ($FieldsArr as $key => $value) {
				$Smarty->assign($key, $value);	
			}
			$content = $Smarty->fetch($TemplatePath);
		}
		else {
			$fh = fopen($TemplatePath, 'r');
			$content = fread($fh, filesize($TemplatePath));
			foreach ($FieldsArr as $key => $value) {
				$content = str_replace("##" . $key . "##", $value, $content);
			}
		}
		# Send Mail
		sendMail($Recipient, $Subject, $content, $From);
		# Set Smarty back to correct Template Directory
		$Smarty->setTemplateDir(TPL_DIR);
	}
}
?>