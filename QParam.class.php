<?php
class QParam {

	public static $fieldsArr;

	public static function clean($fieldsArr) {
		foreach ($fieldsArr as $key => $value) {
			if (intval($value) == "-1" || trim($value) == "") {
				unset($fieldsArr[$key]);
			}
		}
		self::$fieldsArr = $fieldsArr;
		return $fieldsArr;
	}
	public function checkField($checkField, $queryDo, $queryElse="") {
		if (($checkField != "" && !isset(self::$fieldsArr[$checkField]))|| (isset(self::$fieldsArr[$checkField]) && (self::$fieldsArr[$checkField] == -1 || !is_numeric(self::$fieldsArr[$checkField]) && self::$fieldsArr[$checkField] == ""))) {
			return $queryElse;
		}
		else {
			return $queryDo;
		}
	}
	public function check($checkField) {

		if (!isset(self::$fieldsArr[$checkField])) {
			return false;
		}
		if (self::$fieldsArr[$checkField] == "") {
			return false;
		}
		if (self::$fieldsArr[$checkField] == -1) {
			return false;
		}
		return true;
	}
}